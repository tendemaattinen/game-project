﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemies : MonoBehaviour {

	public float maxDistance;
	public float damage;
	public float speed;

	private float travelledDistance;
	private bool changeDirection;

	void Start () {
		travelledDistance = 0;
		changeDirection = true;
	}

	void Update () {
		if (Time.timeScale != 0f) {
			if (changeDirection) {
				if (travelledDistance < maxDistance) {
					Vector2 oldPosition = transform.position;
					transform.Translate (Vector2.right * speed);
					travelledDistance += Vector2.Distance (oldPosition, transform.position);
				} else {
					travelledDistance = 0;
					changeDirection = !changeDirection;
				}
			} else {
				if (travelledDistance < maxDistance) {
					Vector2 oldPosition = transform.position;
					transform.Translate (Vector2.left * speed);
					travelledDistance += Vector2.Distance (oldPosition, transform.position);
				} else {
					travelledDistance = 0;
					changeDirection = !changeDirection;
				}
			}
		}
	}
}
