﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour {

	public float maxDistance;
	public float damage;
	public float speed;
	public int direction;

	Rigidbody2D rb;

	void Start () {
		rb = GetComponent<Rigidbody2D> ();
	}


	//Working without staying on platform (usage of boxcollider)
	/*void Update () {
		if (changeDirection) {
			if (travelledDistance < maxDistance) {
				Vector2 oldPosition = transform.position;
				transform.Translate (Vector2.right * speed);
				travelledDistance += Vector2.Distance (oldPosition, transform.position);
			} else {
				travelledDistance = 0;
				changeDirection = !changeDirection;
			}
		} else {
			if (travelledDistance < maxDistance) {
				Vector2 oldPosition = transform.position;
				transform.Translate (Vector2.left * speed);
				travelledDistance += Vector2.Distance (oldPosition, transform.position);
			} else {
				travelledDistance = 0;
				changeDirection = !changeDirection;
			}
		}
	}*/


	void FixedUpdate() {

		//Moving platform to right
		if (direction == 1) {
			rb.MovePosition (rb.position + Vector2.right * speed * Time.fixedDeltaTime);
		}
		//Moving platform to left
		else if (direction == 2) {
			rb.MovePosition (rb.position + Vector2.left * speed * Time.fixedDeltaTime);
		}
		//Moving platform to up
		else if (direction == 3) {
			rb.MovePosition (rb.position + Vector2.up * speed * Time.fixedDeltaTime);
		}
		//Moving platform to down
		else if (direction == 4) {
			rb.MovePosition (rb.position + Vector2.down * speed * Time.fixedDeltaTime);
		}
	}



	void OnTriggerEnter2D(Collider2D other) {
		//Change direction of platform
		if (other.tag == "TileRestarter") {
			if (direction == 1) {
				direction = 2;
			}
			else if (direction == 2) {
				direction = 1;
			}
			else if (direction == 3) {
				direction = 4;
			}
			else if (direction == 4) {
				direction = 3;
			}
		}
	}
}

