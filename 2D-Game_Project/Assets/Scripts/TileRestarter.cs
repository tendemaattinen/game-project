﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileRestarter : MonoBehaviour {

	void Start () {
		// Disables renderer of restart object
		gameObject.GetComponent<Renderer>().enabled = false;
	}
}
