﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class LevelComp : MonoBehaviour {

	private Text coins;

	void Update() {
		GetCoins ();

	}

	public void GetCoins() {
		GameObject player = GameObject.Find ("Player");
		Player playerScript = player.GetComponent<Player>();
		coins = GameObject.Find ("Coins").GetComponent<Text> ();
		coins.text = playerScript.coins.ToString ();
	}

	public void GetTime() {

	}

}
