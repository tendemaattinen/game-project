﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour {

	private Player player;
	private bool isJumping;

	private void Awake() {
		player = GetComponent<Player>();
	}
		
	void Update () {
		if (!isJumping) {
			isJumping = Input.GetButtonDown("Jump");
		}
	}
		
	void FixedUpdate() {
		float horizontalMovement = Input.GetAxis("Horizontal");
		player.MovePlayer (horizontalMovement, isJumping);
		isJumping = false;
	}

}
