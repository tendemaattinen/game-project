﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Restart : MonoBehaviour {

	void Start() {
		// Disables renderer of restart object
		gameObject.GetComponent<Renderer>().enabled = false;
	}
}
