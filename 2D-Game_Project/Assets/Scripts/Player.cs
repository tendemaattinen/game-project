﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Player : MonoBehaviour {


	public float speed;
	public float maxSpeed;
	public float jumpForce;
	private bool inGround;
	public int health1 = 5;
	static int health;
	public int coins;
	private Rigidbody2D rb2d;
	private Animator m_Anim;
	private Transform gC;
	const float k_GroundedRadius = .2f;
	private Transform cC;
	//const float k_CeilingRadius = .01f;
	//private Transform m_CeilingCheck;
	public LayerMask groundLayer;
	private int jumps = 0;
	private int maxJumps = 1;
	private bool viewingRight = true;
	public Vector2 originalPosition;
	private bool invincible;
	private bool onMovingTile;
	private bool pause = false;
	private bool guiSwitch = false;
	private float levelTime;

	public AudioSource audioSource;
	public AudioClip jumpSound;
	public AudioClip coinSound;
	public AudioClip enemyHitSound;

	private Text healthText;
	private Text coinsText;
	private Text timeText;

	private int coinsPer;

	private int level1coins = 80;
	private int level2coins = 10;

	private int currentLevel;

	void Awake () {
		//DontDestroyOnLoad (transform.gameObject);
		currentLevel = SceneManager.GetActiveScene().buildIndex;
		rb2d = GetComponent<Rigidbody2D> ();
		m_Anim = GetComponent<Animator>();
		gC = transform.Find("GroundCheck");
		cC = transform.Find("CeilingCheck");
		originalPosition = gameObject.transform.position;
		coins = 0;
		health = health1;
		invincible = false;
		onMovingTile = false;
		audioSource = gameObject.AddComponent<AudioSource> ();
		levelTime = 0f;
	}

	void Update() {
		if (Input.GetKeyDown(KeyCode.Escape)) {
			pause = togglePause ();
		}

		levelTime += Time.deltaTime;

		healthText = GameObject.Find ("HealthText").GetComponent<Text>();
		coinsText = GameObject.Find ("CoinText").GetComponent<Text>();
		timeText = GameObject.Find ("TimeText").GetComponent<Text> ();
		healthText.text = health.ToString ();
		coinsText.text = coins.ToString ();
		timeText.text = Mathf.Round(levelTime).ToString ();

	}


	void FixedUpdate () {
		
		inGround = false;

		Collider2D[] colliders = Physics2D.OverlapCircleAll(gC.position, k_GroundedRadius, groundLayer);

		for (int i = 0; i < colliders.Length; i++)
		{
			if (colliders[i].gameObject != gameObject)
				inGround = true;
		}

		if (inGround) {
			jumps = 0;
		}

		m_Anim.SetBool("Ground", inGround);
		m_Anim.SetFloat("vSpeed", rb2d.velocity.y);
	}




	public void MovePlayer(float hm, bool jumping) {

		//Moving
		m_Anim.SetFloat("Speed", Mathf.Abs(hm));
		rb2d.velocity = new Vector2 (hm * maxSpeed, rb2d.velocity.y);

		//Speed limiter
		if (rb2d.velocity.magnitude > maxSpeed) {
			rb2d.velocity = rb2d.velocity.normalized * maxSpeed;
		}

		if (onMovingTile) {
			rb2d.MovePosition (rb2d.position + Vector2.left * 10 * Time.fixedDeltaTime);
			if (rb2d.velocity.magnitude > maxSpeed) {
				rb2d.velocity = rb2d.velocity.normalized * (maxSpeed + 5);
			}

		}

		if (hm < 0 && viewingRight) {
			switchDirect ();
		}

		if (hm > 0 && !viewingRight) {
			switchDirect ();
		}

		//Doublejumping
		if (jumps < maxJumps && jumping) {
			audioSource.PlayOneShot(jumpSound);
			rb2d.AddForce(new Vector2(0f, jumpForce));
			jumps++;
		}

		//Jumping
		if (inGround && jumping && m_Anim.GetBool("Ground")) {
			inGround = false;
			m_Anim.SetBool("Ground", false);
			audioSource.PlayOneShot(jumpSound);
			rb2d.AddForce(new Vector2(0f, jumpForce));
			jumps++;
		}



	}

	bool togglePause() {
		if (Time.timeScale == 0f) {
			Time.timeScale = 1f;
			return(false);
		} else {
			Time.timeScale = 0f;
			return(true);
		}
	}


	private void switchDirect() {
		viewingRight = !viewingRight;
		Vector3 scale = transform.localScale;
		scale.x *= -1;
		transform.localScale = scale;
	}

	private void OnTriggerEnter2D(Collider2D other) {
		if (other.tag == "Coin") {
			other.gameObject.SetActive (false);
			audioSource.PlayOneShot (coinSound);
			coins++;
		}

		if (other.tag == "Enemy") {
			if (!invincible) {
				audioSource.PlayOneShot (enemyHitSound);
				StartCoroutine (EnemyHit ());
			}
		}

		if (other.tag == "Goal") {
			guiSwitch = true;
		}

		if (other.tag == "Restarter") {
			health -= 1;
			if (health < 0) {
				Destroy (gameObject);
				SceneManager.LoadScene(2);
			}

			// Restarts player to start position
			gameObject.transform.position = originalPosition;
		}
	}


	public GUIStyle customstyle;
	public GUIStyle customButton;

	void OnGUI() {
		if (guiSwitch) {
			if (currentLevel == 4) {
				coinsPer = (coins / level1coins) * 100;
			}
			if (currentLevel == 5) {
				coinsPer = (coins / level2coins) * 100;
			}

			Time.timeScale = 0f;
			GUI.Box (new Rect(Screen.width/2, Screen.height/2-150, 150, 25), "Victory", customstyle);
			GUI.Box (new Rect(Screen.width/2, Screen.height/2, 150, 25), "Coins: " + coinsPer + "%", customstyle);
			GUI.Box (new Rect(Screen.width/2, Screen.height/2-75, 150, 25), "Your time: " + levelTime.ToString() + "s", customstyle);
			if (GUI.Button (new Rect(Screen.width/2 , (Screen.height/2) + 75, 150, 100), "Main Menu", customButton)) {
				guiSwitch = false;
				pause = togglePause();
				SceneManager.LoadScene (0);
			}
		}

		if (pause) {
			GUI.Box (new Rect(Screen.width/2, Screen.height/2-150, 150, 25), "Pause-menu", customstyle);
			if (GUI.Button (new Rect(Screen.width/2, Screen.height/2, 150, 100), "Continue", customButton)) {
				pause = togglePause();
			}
			if (GUI.Button (new Rect(Screen.width/2 , (Screen.height/2) + 150, 150, 100), "Main Menu", customButton)) {
				pause = togglePause();
				SceneManager.LoadScene (0);
			}
		}

	}

	IEnumerator EnemyHit() {
		invincible = true;
		health -= 1;
		if (health < 0) {
			Destroy (gameObject);
			SceneManager.LoadScene(2);
		}
		gameObject.GetComponent<Renderer>().enabled = false;
		yield return new WaitForSeconds (0.3f);
		gameObject.GetComponent<Renderer>().enabled = true;
		yield return new WaitForSeconds (0.3f);
		gameObject.GetComponent<Renderer>().enabled = false;
		yield return new WaitForSeconds (0.3f);
		gameObject.GetComponent<Renderer>().enabled = true;
		yield return new WaitForSeconds (0.3f);
		gameObject.GetComponent<Renderer>().enabled = false;
		yield return new WaitForSeconds (0.3f);
		gameObject.GetComponent<Renderer>().enabled = true;
		invincible = false;
	}
}
